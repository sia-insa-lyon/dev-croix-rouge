# Bug discovery

<!--
  Thank you for contributing to this project by creating an issue!
  To avoid duplicate issues we ask you to check off the following list.
-->

<!-- Checked checkbox should look like this: [x] -->

- [ ] The issue is present in the latest release.
- [ ] I have searched the [issues](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/issues) of this repository and believe that this is not a duplicate.

## Current Behavior 😯

<!-- Describe what happens instead of the expected behavior. -->

## Expected Behavior 🤔

<!-- Describe what should happen. -->

## Steps to Reproduce 🕹

<!-- 
Provide a link to a live example and an unambiguous set of steps to reproduce this bug. 
Issues without some form of live example have a longer response time.
-->

Steps:

1.
1.
1.
1.

## (Optional) Context 🔦

<!--
  What are you trying to accomplish? How has this issue affected you?
  Providing context helps us come up with a solution that is most useful in the real world.
-->

## Your Environment 🌎

<!--
  Include as many relevant details about the environment with which you experienced the bug.
  Please complete the following table.
-->
| Env infos        | Version |
| ----------- | ------- |
| Browser name     |         |
| On Smartphone  | :no_entry_sign: or :heavy_check_mark: |

## Quick actions
<!---Don't touch this section unless you know what you do. Theses quick actions are here to enhance the treatment of issues afterwards.--->
/label Priority::High
/label Bug::Discovery

