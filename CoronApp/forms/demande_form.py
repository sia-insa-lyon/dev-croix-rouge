from crispy_forms.bootstrap import TabHolder, Tab, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, Row, Column, Button
from django import forms
from django.urls import reverse

from CoronApp.models import Demande


class DemandeForm(forms.ModelForm):
    class Meta:
        model = Demande
        exclude = ["etat", "created_at", "updated_at", "created_by"]

        datePicker = forms.DateTimeInput(attrs={"class": "datetime-input",})
        widgets = {
            "date_debut": datePicker,
            "date_fin": datePicker,
            "date_presence_debut": datePicker,
            "date_presence_fin": datePicker,
        }

    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = "demande-form"

        self.helper.layout = Layout(
            TabHolder(
                Tab(
                    "Mission",
                    "titre",
                    Row(
                        Column("date_debut", css_class="form-group col-md-6 mb-0"),
                        Column("date_fin", css_class="form-group col-md-6 mb-0"),
                        css_class="form-row",
                    ),
                    "lieu_deroulement",
                    "responsable",
                ),
                Tab(
                    "Effectifs",
                    "nb_benevoles",
                    Row(
                        Column(
                            "date_presence_debut", css_class="form-group col-md-6 mb-0"
                        ),
                        Column(
                            "date_presence_fin", css_class="form-group col-md-6 mb-0"
                        ),
                        css_class="form-row",
                    ),
                    "lieu_rdv",
                    "qualifications",
                ),
                Tab(
                    "Type de mission",
                    "type",
                    Fieldset(
                        "Informations spécifiques à la mission",
                        "nb_PASS",
                        css_class="optional",
                        css_id="maraude",
                    ),
                    Fieldset(
                        "Informations spécifiques à la mission",
                        "nb_colis",
                        css_class="optional",
                        css_id="aide_alim",
                    ),
                    Fieldset(
                        "Informations spécifiques à la mission",
                        Row(
                            Column(
                                "nb_beneficiaire_h",
                                css_class="form-group col-md-4 mb-0",
                            ),
                            Column(
                                "nb_beneficiaire_f",
                                css_class="form-group col-md-4 mb-0",
                            ),
                            Column(
                                "nb_beneficiaire_enfant",
                                css_class="form-group col-md-4 mb-0",
                            ),
                            css_class="form-row",
                        ),
                        "eau_potable",
                        "frigo",
                        "cuisiner",
                        "risque_sanitaire",
                        css_class="optional",
                        css_id="colis_urgence",
                    ),
                    Fieldset(
                        "Informations spécifiques à la mission",
                        Row(
                            Column(
                                "nb_colis_type_1", css_class="form-group col-md-4 mb-0"
                            ),
                            Column(
                                "nb_colis_type_2", css_class="form-group col-md-4 mb-0"
                            ),
                            Column(
                                "nb_colis_type_3", css_class="form-group col-md-4 mb-0"
                            ),
                            css_class="form-row",
                        ),
                        "nb_beneficiaire",
                        "typologie_beneficiaire",
                        css_class="optional",
                        css_id="distribution",
                    ),
                ),
                Tab("Subsistances", "nb_repas", "lieu_repas", "commentaires_repas"),
                Tab(
                    "Véhicules",
                    Row(
                        Column("nb_transport", css_class="form-group col-md-3 mb-0"),
                        Column("nb_log", css_class="form-group col-md-3 mb-0"),
                        Column("nb_vl", css_class="form-group col-md-3 mb-0"),
                        Column("nb_vpsp", css_class="form-group col-md-3 mb-0"),
                        css_class="form-row",
                    ),
                    "commentaire_transport",
                ),
                Tab(
                    "Logistique",
                    "nature_chargement",
                    "est_frais",
                    "est_alimentaire",
                    "moyens_engages",
                ),
                Tab(
                    "Informations complémentaires",
                    "contact_in_situ",
                    "tel_in_situ",
                    "description_detaillee",
                ),
            ),
            FormActions(
                Button("previous", "Précédent", css_class="btn-secondary"),
                Button("next", "Suivant", css_class="btn-secondary"),
                Submit("envoyer", "Envoyer", css_class="float-right"),
            ),
        )

        self.helper.form_method = "post"
        self.helper.form_action = reverse("demande-form")

    # Pour être sûr que les dates ne changent pas à l'édition
    def clean_date_debut(self):
        if self.instance.date_debut and self.instance.etat != "cree":
            return self.instance.date_debut
        return self.cleaned_data["date_debut"]

    def clean_date_fin(self):
        if self.instance.date_fin and self.instance.etat != "cree":
            return self.instance.date_fin
        return self.cleaned_data["date_fin"]

    def clean_date_presence_debut(self):
        if self.instance.date_presence_debut and self.instance.etat != "cree":
            return self.instance.date_presence_debut
        return self.cleaned_data["date_presence_debut"]

    def clean_date_presence_fin(self):
        if self.instance.date_presence_fin and self.instance.etat != "cree":
            return self.instance.date_presence_fin
        return self.cleaned_data["date_presence_fin"]
