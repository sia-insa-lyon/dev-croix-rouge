from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Row, Column
from django import forms

from CoronApp.models.retour import Retour


class RetourForm(forms.ModelForm):
    class Meta:
        model = Retour
        exclude = ["demande", "created_at"]

    def __init__(self, *args, **kwargs):
        super(RetourForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Matériel utilisé",
                Row(
                    Column("nb_masques_chir", css_class="form-group col-md-4 mb-0"),
                    Column("nb_masques_FFP2", css_class="form-group col-md-4 mb-0"),
                    Column("nb_masques_textiles", css_class="form-group col-md-4 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("nb_surblouses", css_class="form-group col-md-6 mb-0"),
                    Column("nb_charlottes", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
            ),
            Fieldset(
                "Retour d'expérience",
                "accident",
                "difficultes",
                "amelioration",
                "commentaire",
            ),
        )
