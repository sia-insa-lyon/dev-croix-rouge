# Generated by Django 3.0.5 on 2020-04-08 09:55

from django.db import migrations
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ("CoronApp", "0005_auto_20200408_1113"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="profil", options={"permissions": [("is_chef", "User is chef")]},
        ),
        migrations.AlterField(
            model_name="demande",
            name="etat",
            field=django_fsm.FSMField(
                choices=[
                    ("refuse", "Refusée"),
                    ("cloture", "Cloturée"),
                    ("cree", "Créée"),
                    ("provisionne", "Provisionnée"),
                    ("valide", "Validée"),
                ],
                default="cree",
                max_length=50,
            ),
        ),
    ]
