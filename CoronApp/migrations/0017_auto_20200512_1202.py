# Generated by Django 3.0.5 on 2020-05-12 10:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("CoronApp", "0016_auto_20200512_1124"),
    ]

    operations = [
        migrations.AlterField(
            model_name="profil",
            name="user",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="profil",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
