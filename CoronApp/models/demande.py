import sys

from django.conf import settings
from django.db import models
from django.utils.html import format_html
from django_fsm import FSMField, transition

from . import reservation
from .profil import Profil
from .retour import *
from .reponse import Reponse

ETATS = {
    ("cree", "Créée"),
    ("valide", "Validée"),
    ("provisionne", "Provisionnée"),
    ("cloture", "Cloturée"),
    ("refuse", "Refusée"),
    ("termine", "Terminée"),
    ("archive", "Archivée"),
}

TYPES = {
    ("logistique", "Logistique"),
    ("maraude", "Maraude sanitaire"),  # Demande particulière
    ("aide_alim", "Aide alimentaire"),  # Demande partiulière
    ("colis_urgence", "Colis d'urgence"),  # Demande particulière
    ("cr_chez_vous", "Croix-Rouge chez vous"),
    ("mission_urgence", "Mission d'urgence"),
    ("accueil_jour", "Accueil de jour"),
    ("secours_publics", "Renfort secours publics"),
    ("desserrement", "Centre de desserrement"),
    ("distribution", "Distribution alimentaire"),  # Demande particulière
    ("continuite", "Continuité des activités"),  # Pas de retour particulier
    ("accueil_ecoute", "Accueil-écoute"),  # Pas de retour particulier
    ("soutien_op", "Soutien à l'opération"),  # Pas de retour particulier
    ("non_defini", "Non précisé"),  # Default
}


class Demande(models.Model):
    class Meta:
        ordering = ["date_debut", "responsable", "titre"]
        permissions = [
            ("can_validate", "Can validate request"),
            ("can_cloture", "Can cloture"),
            ("can_edit", "Can edit a demand"),
            ("can_refuse", "Can refuse a demand"),
            ("can_finish", "Can add feedback"),
            ("can_archive", "Can archive demande"),
        ]

    created_at = models.DateTimeField(
        verbose_name="Date de création de la demande", auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name="Date de dernière édition", auto_now=True
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL
    )
    etat = FSMField(default="cree", choices=ETATS)

    # Infos générales

    titre = models.CharField(verbose_name="Titre de la demande", max_length=128)
    date_debut = models.DateTimeField(
        verbose_name="Date et heure de début de l'opération"
    )
    date_fin = models.DateTimeField(verbose_name="Date et heure de fin de l'opération")
    lieu_deroulement = models.CharField(
        verbose_name="Lieu de la mission", max_length=255
    )
    responsable = models.ForeignKey(
        Profil,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name="Responsable de la mission",
    )
    type = models.CharField(
        verbose_name="Type de mission",
        choices=TYPES,
        default="non_defini",
        max_length=100,
    )

    # Effectifs
    nb_benevoles = models.PositiveIntegerField(
        verbose_name="Nombre de bénévoles demandés", default=0
    )
    date_presence_debut = models.DateTimeField(
        verbose_name="Date et heure du rendez-vous pour les bénévoles"
    )
    date_presence_fin = models.DateTimeField(
        verbose_name="Date et heure de fin de présence pour la mission"
    )
    lieu_rdv = models.CharField(
        verbose_name="Lieu de rendez-vous pour les bénévoles", max_length=255
    )
    qualifications = models.TextField(
        verbose_name="Qualifications spécifiques demandées", blank=True
    )

    # Subsistances
    nb_repas = models.PositiveIntegerField(
        verbose_name="Nombre de repas nécessaires", default=0
    )
    lieu_repas = models.CharField(
        verbose_name="Lieu de la prise de repas", max_length=255, blank=True
    )
    commentaires_repas = models.TextField(
        verbose_name="Commentaires/Régimes alimentaires particuliers", blank=True
    )

    # Vehicules
    nb_transport = models.PositiveIntegerField(
        verbose_name="Bénévoles à transporter", default=0
    )
    nb_log = models.PositiveIntegerField(verbose_name="Nombre de LOG", default=0)
    nb_vl = models.PositiveIntegerField(verbose_name="Nombre de VL", default=0)
    nb_vpsp = models.PositiveIntegerField(verbose_name="Nombre de VPSP", default=0)
    commentaire_transport = models.TextField(
        verbose_name="Demande spécifique", blank=True
    )

    # Logistique
    nature_chargement = models.CharField(
        verbose_name="Nature du chargement",
        help_text="Taille, poids, conditionnement",
        max_length=255,
        blank=True,
    )
    est_alimentaire = models.BooleanField(
        verbose_name="Contient de l'alimentaire", default=False
    )
    est_frais = models.BooleanField(verbose_name="Frais", default=False)
    moyens_engages = models.TextField(
        verbose_name="Moyens engagés",
        help_text="[type, nombre, utilisation], [secours, urgence (CAI/CHU/etc.), électriques ("
        "chauffage/GPE/rallonges/lampes/etc.), soutien (tables/chaise/etc.)]",
        blank=True,
    )

    # Infos complémentaires
    contact_in_situ = models.CharField(
        verbose_name="Contact sur place", max_length=255, blank=True
    )
    tel_in_situ = models.CharField(
        verbose_name="Téléphone du contact sur place", max_length=20, blank=True
    )
    description_detaillee = models.TextField(
        verbose_name="Description détaillée de la mission",
        help_text="Chronogramme précis, transport, mise en place etc.",
    )

    # Infos spécifique à un type de demande
    # Pas de classes dérivées pour les demandes spécifiques parce que ce serait trop compliqué pour la suite,
    # les infos sont juste blank sinon, vu le volume de données c'est pas trop gros
    # Pour les retours en revanche on utilise de la composition

    # Maraude sanitaire
    nb_PASS = models.PositiveIntegerField(
        verbose_name="Nombre total de personnel PASS", blank=True, default=0
    )

    # Aide alimentaire
    nb_colis = models.PositiveIntegerField(
        verbose_name="Nombre de colis d'urgence", blank=True, default=0
    )

    # Colis d'urgence
    nb_beneficiaire_h = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires homme", blank=True, default=0
    )
    nb_beneficiaire_f = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires femme", blank=True, default=0
    )
    nb_beneficiaire_enfant = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires enfant", blank=True, default=0
    )
    eau_potable = models.BooleanField(
        verbose_name="Accès eau potable", blank=True, default=False
    )
    frigo = models.BooleanField(
        verbose_name="Présence de frigo", blank=True, default=False
    )
    cuisiner = models.BooleanField(
        verbose_name="Possibilité de cuisiner", blank=True, default=False
    )
    risque_sanitaire = models.TextField(verbose_name="Risque sanitaire", blank=True)

    # Distribution alimentaire
    nb_colis_type_1 = models.PositiveIntegerField(
        verbose_name="Nombre de colis de type 1", blank=True, default=0
    )
    nb_colis_type_2 = models.PositiveIntegerField(
        verbose_name="Nombre de colis de type 2", blank=True, default=0
    )
    nb_colis_type_3 = models.PositiveIntegerField(
        verbose_name="Nombre de colis de type 3", blank=True, default=0
    )
    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires", blank=True, default=0
    )
    typologie_beneficiaire = models.TextField(
        verbose_name="Typologie des bénéficiaires", blank=True
    )

    # Propriétée utilisée dans l'admin Django pour colorer les états des demandes
    def colored_etat(self):
        return format_html(
            '<span class="color_{}">{}</span>', self.etat, self.get_etat_display()
        )

    colored_etat.short_description = "Etat"

    @transition(
        field=etat,
        source="cree",
        target="valide",
        permission=lambda instance, user: user.hasPerm("CoronApp.can_validate"),
    )
    def validate(self):
        r = Reponse(demande=self)
        r.save()

    def est_provisionne(self):
        if (
            self.reponse.effectifs_provisionnes
            and self.reponse.logistique_provisionnes
            and self.reponse.subsistances_provisionnes
            and self.reponse.vehicules_provisionnes
        ):
            return True
        return False

    @transition(
        field=etat, source="valide", target="provisionne", conditions=[est_provisionne]
    )
    def provisionne(self):
        pass

    @transition(field=etat, source="cree", target="refuse")
    def refuse(self):
        pass

    @transition(
        field=etat,
        source="provisionne",
        target="cloture",
        permission=lambda instance, user: user.hasPerm("CoronApp.can_cloture"),
    )
    def cloture(self):
        ret = Retour(demande=self)
        ret.save()

    @transition(field=etat, source="cloture", target="termine")
    def termine(self):
        pass

    @transition(field=etat, source=["termine", "cloture", "refuse"], target="archive")
    def archive(self):
        pass

    def has_retour_spe(self):
        """ Seuls quelques types de mission n'ont pas de retour spéciique, cette fonction sert pour le template
        et retourne vrai si la demande a une retour spécifique
        """

        return self.type not in [
            "non_defini",
            "continuite",
            "accueil_ecoute",
            "soutien_op",
        ]

    def has_demande_spe(self):
        """ Idem du côté de la demande spécifique : 4 types ont des spécificités """
        return self.type in ["maraude", "aide_alim", "colis_urgence", "distribution"]

    def __str__(self):
        return "Demande " + self.titre
