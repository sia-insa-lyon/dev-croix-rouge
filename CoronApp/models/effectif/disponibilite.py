from django.db import models


class Disponibilite(models.Model):
    debut = models.DateTimeField(verbose_name="date et heure de début")
    fin = models.DateTimeField(verbose_name="date et heure de fin")

    def __str__(self):
        return "disponibilité de " + str(self.debut) + " à " + str(self.fin)
