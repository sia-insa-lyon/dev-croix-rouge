from django.db import models


class LieuStockage(models.Model):
    label = models.CharField(verbose_name="nom du lieu de stockage", max_length=128)

    def __str__(self):
        return self.label
