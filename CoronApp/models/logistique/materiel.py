from django.db import models
from CoronApp.models.logistique.logistique import Logistique
from CoronApp.models.ressource import RessourcePointer


class Materiel(Logistique):
    ressource = models.OneToOneField(
        RessourcePointer, on_delete=models.CASCADE, default=None, blank=True
    )
    typeMateriel = models.ForeignKey(
        "CoronApp.TypeMateriel",
        verbose_name="type de matériel",
        help_text="Ex: Rallonge, lampe",
        on_delete=models.SET_NULL,
        null=True,
    )
    categorieMateriel = models.ForeignKey(
        "CoronApp.CategorieMateriel",
        verbose_name="Catégorie de matériel",
        help_text="Ex: Secours, elec",
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return (
            "Materiel : "
            + self.denomination
            + " ("
            + str(self.typeMateriel)
            + ") | "
            + self.lieu.__str__()
        )


class CategorieMateriel(models.Model):
    """
    Exemples : SECOURS, ELEC etc.
    """

    label = models.CharField(
        verbose_name="catégorie du materiel",
        help_text="Ex: Secours, elec",
        max_length=128,
    )

    def __str__(self):
        return self.label


class TypeMateriel(models.Model):
    """
    Exemples : RALLONGE, LAMPE etc.
    """

    label = models.CharField(
        verbose_name="nom du type de materiel",
        help_text="Ex: Rallonge, lampe",
        max_length=128,
    )

    def __str__(self):
        return self.label
