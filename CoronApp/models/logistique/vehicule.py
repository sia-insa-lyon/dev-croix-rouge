from django.db import models
from CoronApp.models.logistique.logistique import Logistique
from CoronApp.models.ressource import RessourcePointer


class Vehicule(Logistique):
    ressource = models.OneToOneField(
        RessourcePointer, on_delete=models.CASCADE, default=None, blank=True
    )
    typeVehicule = models.ForeignKey(
        "CoronApp.TypeVehicule", on_delete=models.SET_NULL, null=True
    )

    categorieVehicule = models.ForeignKey(
        "CoronApp.CategorieVehicule", on_delete=models.SET_NULL, null=True
    )

    nbPlacesNormales = models.PositiveIntegerField(
        verbose_name="nombre de places normales", default=1
    )
    nbPlacesCovid = models.PositiveIntegerField(
        verbose_name="nombre de places covid", default=1
    )
    immatriculation = models.CharField(
        verbose_name="immatriculation du véhicule", blank=True, null=True, max_length=30
    )

    def __str__(self):
        return (
            "Vehicule : "
            + self.denomination
            + " ("
            + str(self.typeVehicule)
            + ") | "
            + self.lieu.__str__()
        )


class CategorieVehicule(models.Model):
    """
    Exemples : VEHICULE, REMORQUE etc.
    """

    label = models.CharField(
        verbose_name="catégorie du véhicule", max_length=128, unique=True
    )

    def __str__(self):
        return self.label


class TypeVehicule(models.Model):
    """
    Exemples : VL, MINIBUS etc.
    """

    label = models.CharField(
        verbose_name="nom du type du véhicule", max_length=128, unique=True
    )

    def __str__(self):
        return self.label
