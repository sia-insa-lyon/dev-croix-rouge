# coding=utf-8
from django.db import models

# Create your models here.


class Reponse(models.Model):
    class Meta:
        permissions = [
            ("can_assign_vehicules", "Can assign vehicules"),
            ("can_assign_effectifs", "Can assign effectifs"),
            ("can_assign_subsistances", "Can assign subsistances"),
            ("can_assign_logistique", "Can assign logistique"),
            ("can_comment", "Can comment"),
        ]

    demande = models.OneToOneField(
        "CoronApp.Demande",
        on_delete=models.CASCADE,
        verbose_name="Demande",
        rel="reponse",
    )

    # Effectifs
    commentaire_effectifs = models.TextField(
        verbose_name="Commentaire assignation effectifs", blank=True
    )
    effectifs_provisionnes = models.BooleanField(
        verbose_name="Effectifs provisionnés", default=False
    )

    # Véhicules
    commentaire_vehicules = models.TextField(
        verbose_name="Commentaire assignation véhicules", blank=True
    )  # TODO à supprimer
    vehicules_provisionnes = models.BooleanField(
        verbose_name="Véhicules provisionnés", default=False
    )

    # Logistique
    logistique = models.TextField(
        verbose_name="Logistique", blank=True
    )  # TODO à supprimer
    logistique_provisionnes = models.BooleanField(
        verbose_name="Logistique provisionnée", default=False
    )

    # Subsitances
    subsistances = models.TextField(verbose_name="Subsistances", blank=True)
    subsistances_provisionnes = models.BooleanField(
        verbose_name="Subsistances provisionnées", default=False
    )

    commentaires = models.TextField(verbose_name="Commentaires", blank=True)

    def provisionneVehicule(self):
        self.vehicules_provisionnes = True
        self.save()
        # TODO gérer les réservation
        try:
            self.demande.provisionne()
            self.demande.save()
        except:
            pass

    def provisionneLogistique(self):
        self.logistique_provisionnes = True
        self.save()
        # TODO gérer les réservation
        try:
            self.demande.provisionne()
            self.demande.save()
        except:
            pass

    def provisionneSubsistance(self):
        self.subsistances_provisionnes = True
        self.save()
        # TODO faire plus propre
        try:
            self.demande.provisionne()
            self.demande.save()
        except:
            pass

    def provisionneEffectif(self):
        self.effectifs_provisionnes = True
        self.save()
        # TODO gérer les réservation
        try:
            self.demande.provisionne()
            self.demande.save()
        except:
            pass

    def __str__(self):
        return self.demande.titre
