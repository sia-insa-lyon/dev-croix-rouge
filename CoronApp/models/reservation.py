# coding=utf-8
from django.db import models


class Reservation(models.Model):
    quantite = models.PositiveIntegerField(verbose_name="quantité", default=1,)
    reponse = models.ForeignKey(
        "CoronApp.Reponse", on_delete=models.CASCADE, related_name="reservations"
    )
    ressource = models.ForeignKey(
        "CoronApp.RessourcePointer",
        verbose_name="ressource réservée",
        on_delete=models.SET_NULL,
        blank=True,
        default=None,
        null=True,
    )

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        if (hasattr(self.ressource, "effectif1j")) or (
            hasattr(self.ressource, "effectifcrf")
        ):
            self.quantite = 1

        super(Reservation, self).save(force_insert, force_update, using, update_fields)
