# coding=utf-8
import sys

from django.db import models


class Retour(models.Model):
    # Pas de permissions particulières pour l'édition du retour, qui
    # est réalisée obligatoirement par le responsable de la mission

    demande = models.OneToOneField(
        "CoronApp.Demande", on_delete=models.CASCADE, rel="retour"
    )
    created_at = models.DateTimeField(verbose_name="Date de création", auto_now=True)

    # Matériel
    nb_masques_chir = models.PositiveIntegerField(
        verbose_name="Masques chirurgicaux consommés", default=0
    )
    nb_masques_FFP2 = models.PositiveIntegerField(
        verbose_name="Masques FFP2 consommés", default=0
    )
    nb_masques_textiles = models.PositiveIntegerField(
        verbose_name="Masques textiles consommés", default=0
    )
    nb_surblouses = models.PositiveIntegerField(
        verbose_name="Quantité de surblouses consommées", default=0
    )
    nb_charlottes = models.PositiveIntegerField(
        verbose_name="Quantité de charlottes consommées", default=0
    )

    # Retour d'expérience
    accident = models.BooleanField(
        verbose_name="Y a-t-il eu accident/maladie d'un bénévole ?", default=False
    )
    difficultes = models.TextField(verbose_name="Difficultés rencontrées", blank=True)
    amelioration = models.TextField(verbose_name="Axes d'amélioration", blank=True)
    commentaire = models.TextField(
        verbose_name="Commentaire de retour de mission", blank=True
    )

    def get_spe_class_name(self):
        """ Les retours spécifiques ont des noms de classe correspondant au type de la mission
        Par exemple, si le type de mission est "aide_alim", la le modèle de retour correspondant sera "RetourAideAlim"
        La fonction suivante réalise la conversion de snake_case vers CamelCase
        pour obtenir le nom de classe de retour à partir du type et retourne un string
        """

        return "Retour" + "".join(
            mot.capitalize() for mot in self.demande.type.split("_")
        )

    def get_retour_spe(self):
        """ get_retour_spe retourne l'instance de retour spécifique associée au retour,
        en la créant si elle n'existe pas
        """

        # L'attribut associé à la relation OneToOne a pour nom le nom du modèle associé en minuscule
        if not hasattr(self, self.get_spe_class_name().lower()):
            # Boulette dans la MR précédente, le type par défaut était une chaine vide, ce qui pose problème ici,
            # car get_spe_class_name ramènerait vers Retour, ce qu'on ne veut absoluement pas.
            # On met à la place non_defini, la classe de retour spécifique associée est RetourNonDefini,
            # qui n'a pas d'attributs
            if self.demande.type == "":
                self.demande.type = "non_defini"

            # Permet de retourner la classe à partir d'un string
            spe = getattr(sys.modules[__name__], self.get_spe_class_name())(retour=self)
            spe.save()

        return getattr(self, self.get_spe_class_name().lower())

    def get_spe_form_class_name(self):
        """ La classe du formulaire associé au retour spécifique est de la forme <classeRetourSpecifique>Form
        A cause d'un problème d'import circulaire, il est impossible d'obtenir depuis ici
        la classe de formulaire associé au retour spécifique, donc on retourne seulement le nom,
        la classe sera retournée dans une fonction dédiée dans la view
        """

        return self.get_spe_class_name() + "Form"

    def __str__(self):
        return "Retour " + self.demande.titre


"""Retours spécifiques : fonctionnement par composition
Chaque retour spécifique est en relation avec un seul retour
"""


class RetourLogistique(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    dons_alim = models.PositiveIntegerField(
        verbose_name="Quantité de dons alimentaire transportés (kg)", default=0
    )
    dons_hygiene = models.PositiveIntegerField(
        verbose_name="Quantité de dons hygiène transportés (kg)", default=0
    )
    dons_EPI = models.PositiveIntegerField(
        verbose_name="Quantité de dons EPI transportés (kg)", default=0
    )
    dons_autres = models.PositiveIntegerField(
        verbose_name="Quantité de dons autres transportés (kg)", default=0
    )


class RetourMaraude(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_beneficiaire_rencontres = models.PositiveIntegerField(
        "Nombre de bénéficiaires rencontrés", default=0
    )


class RetourAideAlim(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_colis_distrib = models.PositiveIntegerField(
        verbose_name="Nombre de colis d'urgence distribués", default=0
    )


class RetourColisUrgence(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre total de bénéficiaires", default=0
    )
    nb_colis = models.PositiveIntegerField(
        verbose_name="Nombre total de colis distribués", default=0
    )
    nb_beneficiaires_inscrits = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires inscrits au suivi", default=0
    )


class RetourCrChezVous(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_livraisons_effectuees = models.PositiveIntegerField(
        verbose_name="Nombre de livraisons effectuées", default=0
    )
    nb_livraisons_echouees = models.PositiveIntegerField(
        verbose_name="Nombre de livraisons échouées", default=0
    )
    nb_paniers = models.PositiveIntegerField(
        verbose_name="Nombre de paniers solidaires", default=0
    )
    signalements_transmis = models.BooleanField(
        verbose_name="Signalements transmis à la Veille Sociale", default=False
    )


class RetourMissionUrgence(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_lots_PAI = models.PositiveIntegerField(
        verbose_name="Nombre de lots PAI engagés", default=0
    )
    nb_lots_CAI = models.PositiveIntegerField(
        verbose_name="Nombre de lots CAI engagés", default=0
    )
    nb_lots_CHU = models.PositiveIntegerField(
        verbose_name="Nombre de lots CHU engagés", default=0
    )
    nb_lots_CMCC = models.PositiveIntegerField(
        verbose_name="Nombre de lots CMCC engagés", default=0
    )
    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires accueillis", default=0
    )


class RetourAccueilJour(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires accueillis", default=0
    )


class RetourSecoursPublics(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_victimes_vues = models.PositiveIntegerField(
        verbose_name="Nombre de victimes vues", default=0
    )
    nb_interventions_eventuelles = models.PositiveIntegerField(
        verbose_name="Nombre d'interventions éventuelles", default=0
    )
    nb_evac_CRF = models.PositiveIntegerField(
        verbose_name="Nombre d'évacuations CRF", default=0
    )
    nb_evac_non_CRF = models.PositiveIntegerField(
        verbose_name="Nombre d'évacuations hors-CRF", default=0
    )
    nb_UA = models.PositiveIntegerField(
        verbose_name="Nombre d'UA prises en charge", default=0
    )


class RetourDesserrement(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre de bénéficiaires accueillis ce jour", default=0
    )
    nb_VPSP_non_CRF = models.PositiveIntegerField(
        verbose_name="Transports entrant VPSP HORS CRF", default=0
    )
    nb_VPSP_CRF = models.PositiveIntegerField(
        verbose_name="Transports entrant VPSP CRF", default=0
    )
    nb_hors_VPSP = models.PositiveIntegerField(
        verbose_name="Transports entrant HORS VPSP", default=0
    )


class RetourDistribution(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)

    nb_colis = models.PositiveIntegerField(
        verbose_name="Nombre de colis distribués", default=0
    )
    nb_beneficiaire = models.PositiveIntegerField(
        verbose_name="Nombre bénéficiaires rencontrés", default=0
    )


# Pour des raisons de simplicité, même lorsque pas de retour spécifique, on créé une classe
class RetourContinuite(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)


class RetourAccueilEcoute(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)


class RetourSoutienOp(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)


class RetourNonDefini(models.Model):
    retour = models.OneToOneField(Retour, on_delete=models.CASCADE, blank=True)
