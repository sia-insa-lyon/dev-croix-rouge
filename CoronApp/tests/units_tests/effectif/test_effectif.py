from datetime import datetime, timezone
from unittest import skip

from django.test import TestCase

from CoronApp.models import Demande
from CoronApp.models.effectif.disponibilite import Disponibilite
from CoronApp.models.effectif.effectif import Effectif1J, IntersectionDisponibiliteError


class TestEffectif(TestCase):
    def setUp(self):
        # Initialisation d'une demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.t10h = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.t12h = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.date_presence_debut = self.t10h
        self.demande.date_presence_fin = self.t12h
        self.effectif = Effectif1J(
            nom="Dupont",
            prenom="Jean",
            email="jean.dupont@email.fr",
            telephone="0653456787",
        )
        self.effectif.save()

    def test_pas_de_dispo(self):

        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_incluse(self):
        t11h = datetime(2020, 4, 21, 11, 00, tzinfo=timezone.utc)
        t11h30 = datetime(2020, 4, 21, 11, 30, tzinfo=timezone.utc)
        self.effectif.disponibilites.add(Disponibilite(debut=t11h, fin=t11h30).save())
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_exclus(self):
        t9h = datetime(2020, 4, 21, 9, 00, tzinfo=timezone.utc)
        t13 = datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=t9h, fin=t13)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertTrue(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertTrue(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_pile(self):
        dispo = Disponibilite(debut=self.t10h, fin=self.t12h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertTrue(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertTrue(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_exclus_au_dessus(self):
        t13h = datetime(2020, 4, 21, 13, 0, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=self.t10h, fin=t13h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertTrue(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertTrue(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_exclus_en_dessous(self):
        t9h = datetime(2020, 4, 21, 9, 0, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=t9h, fin=self.t12h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertTrue(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertTrue(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_inclus(self):
        t10h01 = datetime(2020, 4, 21, 10, 1, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=t10h01, fin=self.t12h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

        t11h59 = datetime(2020, 4, 21, 11, 59, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=self.t10h, fin=t11h59)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_incluse_en_dessous(self):
        t13h = datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=self.t12h, fin=t13h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_en_dessous(self):
        t9h = datetime(2020, 4, 21, 9, 00, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=t9h, fin=self.t10h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))

    def test_dispo_a_cheval(self):
        t11h = datetime(2020, 4, 21, 11, 00, tzinfo=timezone.utc)
        t13h = datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc)
        dispo = Disponibilite(debut=t11h, fin=t13h)
        dispo.save()
        self.effectif.disponibilites.add(dispo)
        self.assertFalse(
            self.effectif.est_dispo(date_debut=self.t10h, date_fin=self.t12h)
        )
        self.assertFalse(self.effectif.est_dispo(demande=self.demande))
