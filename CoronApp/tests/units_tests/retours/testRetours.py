from datetime import datetime, timezone
from django.test import TestCase
from CoronApp.models import (
    Demande,
    Retour,
    sys,
    RetourSoutienOp,
    RetourAccueilEcoute,
    RetourNonDefini,
)
from CoronApp.models.ressource import RessourcePointer


class RetoursTestCase(TestCase):
    def setUp(self):
        ressource_pointer = RessourcePointer()
        ressource_pointer.save()

        # Initialisation de la demande
        self.demande = Demande(
            titre="Croix Rouge chez vous",
            date_debut=datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc),
            date_fin=datetime(2020, 4, 21, 18, 00, tzinfo=timezone.utc),
        )

        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )

    def test_get_spe_class_name_simple(self):
        self.demande.type = Demande.type = "logistique"
        self.retour = Retour(demande=self.demande)
        self.assertEqual(self.retour.get_spe_class_name(), "RetourLogistique")

    def test_get_spe_class_name_complexe(self):
        self.demande.type = Demande.type = "cr_chez_vous"
        self.retour = Retour(demande=self.demande)
        self.assertEqual(self.retour.get_spe_class_name(), "RetourCrChezVous")

    def test_get_retour_spe_exist_pas_en_db(self):
        self.demande.type = Demande.type = "accueil_ecoute"
        self.demande.save()
        self.retour = Retour(demande=self.demande)
        self.retour.save()

        self.assertEqual(RetourAccueilEcoute.objects.count(), 0)

        self.retourAccueilEcoute = self.retour.get_retour_spe()
        self.assertEqual(type(self.retourAccueilEcoute).__name__, "RetourAccueilEcoute")

        # Un objet a bien été créé en DB
        self.assertEqual(RetourAccueilEcoute.objects.count(), 1)
        self.assertEqual(RetourAccueilEcoute.objects.all()[0], self.retourAccueilEcoute)

    def test_get_retour_spe_exist_en_db(self):
        self.demande.type = Demande.type = "soutien_op"
        self.demande.save()
        self.retour = Retour(demande=self.demande)
        self.retour.save()

        self.retourSoutienOp = RetourSoutienOp()
        self.retourSoutienOp.retour = self.retour
        self.retourSoutienOp.save()

        self.retour.RetourSoutienOp = self.retourSoutienOp
        self.retour.save()

        # Le seul RetourSoutienOp en DB est celui qui existe déjà

        self.assertEqual(RetourSoutienOp.objects.all()[0], self.retourSoutienOp)
        self.assertEqual(type(self.retour.get_retour_spe()).__name__, "RetourSoutienOp")

        # Aucun RetourSoutienOp n'a été créé en DB
        self.assertEqual(RetourSoutienOp.objects.all()[0], self.retourSoutienOp)
        self.assertEqual(RetourSoutienOp.objects.count(), 1)

    def test_get_retour_spe_non_defini(self):
        self.demande.save()
        self.retour = Retour(demande=self.demande)
        self.retour.save()

        self.assertEqual(RetourNonDefini.objects.count(), 0)

        self.retourNonDefini = self.retour.get_retour_spe()
        self.assertEqual(type(self.retourNonDefini).__name__, "RetourNonDefini")

        # Un objet a bien été créé en DB
        self.assertEqual(RetourNonDefini.objects.count(), 1)
        self.assertEqual(RetourNonDefini.objects.all()[0], self.retourNonDefini)

    def test_get_spe_form_class_name(self):
        self.demande.type = Demande.type = "colis_urgence"
        self.retour = Retour(demande=self.demande)
        self.assertEqual(
            self.retour.get_spe_form_class_name(), "RetourColisUrgenceForm"
        )
