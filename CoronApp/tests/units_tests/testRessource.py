from datetime import timezone, datetime
from CoronApp.models import Demande, Reponse, Reservation
from CoronApp.models.effectif.effectif import Effectif1J, EffectifCRF
from CoronApp.models.logistique.vehicule import Vehicule
from CoronApp.models.ressource import RessourcePointer
from django.test import TestCase


# Test de la classe abstraite Ressources
class RessourceTestCase(TestCase):
    def setUp(self):
        ressource_pointer = RessourcePointer()
        ressource_pointer.save()
        self.clio = Vehicule(ressource=ressource_pointer, denomination="Clio")
        self.clio.save()

        # Initialisation de la demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )

    def test_aucune_resa(self):
        self.assertTrue(
            self.clio.est_dispo(
                datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )

    def test_vehicule(self):
        self.clio.qteDispo = 0
        self.assertFalse(
            self.clio.est_dispo(
                datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )
        self.clio.qteDispo = 1

    def test_une_resa_dispo(self):
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.save()

        self.response = Reponse(demande=self.demande)
        self.response.save()

        self.assertTrue(
            self.clio.est_dispo(
                date_debut=datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                date_fin=datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )

    def test_une_resa_indispo(self):
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 16, 00, tzinfo=timezone.utc)
        self.demande.save()

        self.response = Reponse(demande=self.demande)
        self.response.save()

        reservation = Reservation(
            quantite=1, reponse=self.response, ressource=self.clio.ressource
        )
        reservation.save()
        self.assertFalse(
            self.clio.est_dispo(
                date_debut=datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                date_fin=datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )

    def test_une_resa_indispo_entre(self):
        self.demande.date_debut = datetime(2020, 4, 21, 14, 30, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 14, 40, tzinfo=timezone.utc)
        self.demande.save()

        self.response = Reponse(demande=self.demande)
        self.response.save()

        reservation = Reservation(
            quantite=1, reponse=self.response, ressource=self.clio.ressource
        )
        reservation.save()
        self.assertFalse(
            self.clio.est_dispo(
                date_debut=datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                date_fin=datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )

    def test_une_resa_dispo_juste_avant(self):
        self.demande.date_debut = datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc)
        self.demande.save()

        self.response = Reponse(demande=self.demande)
        self.response.save()

        resa = Reservation(
            quantite=1, reponse=self.response, ressource=self.clio.ressource
        )
        resa.save()
        self.assertTrue(
            self.clio.est_dispo(
                date_debut=datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
                date_fin=datetime(2020, 4, 21, 14, 55, tzinfo=timezone.utc),
            )
        )

    def test_une_resa_dispo_meme_creneau(self):
        self.demande.date_debut = datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc)
        self.demande.save()

        self.response = Reponse(demande=self.demande)
        self.response.save()

        reservation = Reservation(
            quantite=1, reponse=self.response, ressource=self.clio.ressource
        )
        reservation.save()
        self.assertFalse(
            self.clio.est_dispo(
                date_debut=datetime(2020, 4, 21, 13, 30, tzinfo=timezone.utc),
                date_fin=datetime(2020, 4, 21, 14, 25, tzinfo=timezone.utc),
            )
        )


class test_ressource_pointer(TestCase):
    def test_ressource_pointer_vehicule(self):
        # Véhicule
        vehicule = Vehicule(denomination="kangoo", nbPlacesCovid=2, nbPlacesNormales=3)
        self.assertFalse(hasattr(vehicule, "ressource"))
        vehicule.save()
        self.assertTrue(hasattr(vehicule, "ressource"))
        ressource_pointer = vehicule.ressource
        vehicule.delete()
        self.assertEqual(
            RessourcePointer.objects.filter(id=ressource_pointer.id).count(), 0
        )

    def test_ressource_pointer_effectif(self):
        # Effectif 1J
        effectif = Effectif1J(
            nom="Dupont",
            prenom="Jean",
            email="jean.dupont@email.fr",
            telephone="0653456787",
        )
        self.assertFalse(hasattr(effectif, "ressource"))
        effectif.save()
        self.assertTrue(hasattr(effectif, "ressource"))
        ressource_pointer = effectif.ressource
        effectif.delete()
        self.assertEqual(
            RessourcePointer.objects.filter(id=ressource_pointer.id).count(), 0
        )

        # EffectifCRF
        effectif = EffectifCRF(
            nom="Dupont",
            prenom="Jean",
            email="jean.dupont@email.fr",
            telephone="0653456787",
            NIVOL="64674",
        )
        self.assertFalse(hasattr(effectif, "ressource"))
        effectif.save()
        self.assertTrue(hasattr(effectif, "ressource"))
        ressource_pointer = effectif.ressource
        effectif.delete()
        self.assertEqual(
            RessourcePointer.objects.filter(id=ressource_pointer.id).count(), 0
        )
