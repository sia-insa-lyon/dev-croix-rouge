from django.test import TestCase
from CoronApp.models import Reponse, Demande
from datetime import datetime, timezone


class ResponseTestCase(TestCase):
    def setUp(self):
        demande = Demande(
            titre="Demande pour tests Reponse",
            date_debut=datetime(2020, 5, 4, tzinfo=timezone.utc),
            date_fin=datetime(2020, 5, 5, tzinfo=timezone.utc),
            lieu_deroulement="Villeurbanne centre",
            date_presence_debut=datetime(2020, 5, 4, tzinfo=timezone.utc),
            date_presence_fin=datetime(2020, 5, 5, tzinfo=timezone.utc),
            lieu_rdv="184 Avenue je sais pas où",
            description_detaillee="Demande pour les tests, c'est plus détaillé",
        )
        demande.save()

        self.reponse = Reponse(demande=demande)
        self.reponse.save()

    def test_provision_vehicule(self):
        self.reponse.provisionneVehicule()

        self.assertTrue(self.reponse.vehicules_provisionnes)
        self.assertFalse(self.reponse.logistique_provisionnes)
        self.assertFalse(self.reponse.subsistances_provisionnes)
        self.assertFalse(self.reponse.effectifs_provisionnes)

    def test_provision_materiel(self):
        self.reponse.provisionneLogistique()

        self.assertFalse(self.reponse.vehicules_provisionnes)
        self.assertTrue(self.reponse.logistique_provisionnes)
        self.assertFalse(self.reponse.subsistances_provisionnes)
        self.assertFalse(self.reponse.effectifs_provisionnes)

    def test_provision_subsistance(self):
        self.reponse.provisionneSubsistance()

        self.assertFalse(self.reponse.vehicules_provisionnes)
        self.assertFalse(self.reponse.logistique_provisionnes)
        self.assertTrue(self.reponse.subsistances_provisionnes)
        self.assertFalse(self.reponse.effectifs_provisionnes)

    def test_provision_effectif(self):
        self.reponse.provisionneEffectif()

        self.assertFalse(self.reponse.vehicules_provisionnes)
        self.assertFalse(self.reponse.logistique_provisionnes)
        self.assertFalse(self.reponse.subsistances_provisionnes)
        self.assertTrue(self.reponse.effectifs_provisionnes)
