""""
Test pour la version 2.0 de l'appli. Ne comprends pas la gestion des ressources
"""

from datetime import datetime, timezone

from django.contrib.auth.models import User

from CoronApp.models import Demande, Profil
from CoronApp.tests.views_tests.endpoint_test import AssignationTest, EndpointTest


class TestAssignationsVehicules(AssignationTest):
    __test__ = True
    type = "vehicules"
    pass


class TestAssignationsEffectifs(AssignationTest):
    __test__ = True
    type = "effectifs"
    pass


class TestAssignationsLogistique(EndpointTest):
    __test__ = True
    permission_name = "can_assign_logistique"

    def setUp(self):
        # Responsable
        self.responsable = User(username="responsable", password="mdp")
        self.responsable.save()

        self.responsable_profil = Profil(user=self.responsable, tel="067557756")
        self.responsable_profil.save()

        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.responsable = self.responsable_profil
        self.demande.save()
        self.demande.validate()
        self.demande.save()

        self.url = "/demande/" + str(self.demande.id) + "/assign/logistique"


class TestAssignationsSubsistances(EndpointTest):
    __test__ = True
    permission_name = "can_assign_subsistances"

    def setUp(self):
        # Responsable
        self.responsable = User(username="responsable", password="mdp")
        self.responsable.save()

        self.responsable_profil = Profil(user=self.responsable, tel="067557756")
        self.responsable_profil.save()

        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.responsable = self.responsable_profil
        self.demande.save()
        self.demande.validate()
        self.demande.save()

        self.url = "/demande/" + str(self.demande.id) + "/assign/subsistances"
