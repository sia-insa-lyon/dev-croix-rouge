import json
from datetime import datetime, timezone

from django.contrib.auth.models import User, Permission
from django.core.exceptions import PermissionDenied
from django.test import TestCase, Client

from CoronApp.forms import RetourForm, RetourNonDefiniForm, RetourDesserrementForm
from CoronApp.models import Demande, Profil, RetourNonDefini


class TestRetour(TestCase):
    def setUp(self):
        self.client = Client()

        # Random user
        self.random_user = User(username="user", password="mdp")
        self.random_user.save()

        self.random_profil = Profil(user=self.random_user, tel="067557756")
        self.random_profil.save()

        # Responsable
        self.responsable = User(username="responsable", password="mdp")
        self.responsable.save()

        self.responsable_profil = Profil(user=self.responsable, tel="067557756")
        self.responsable_profil.save()

        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.responsable = self.responsable_profil
        self.demande.save()
        self.demande.validate()
        self.demande.reponse.provisionneEffectif()
        self.demande.reponse.provisionneVehicule()
        self.demande.reponse.provisionneLogistique()
        self.demande.reponse.provisionneSubsistance()
        self.demande.cloture()
        self.demande.save()

        # Demande avec retour specifique
        self.demande2 = Demande(titre="Croix Rouge chez vous")
        self.demande2.type = "desserrement"
        self.demande2.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande2.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande2.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande2.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande2.responsable = self.responsable_profil
        self.demande2.save()
        self.demande2.validate()
        self.demande2.reponse.provisionneEffectif()
        self.demande2.reponse.provisionneVehicule()
        self.demande2.reponse.provisionneLogistique()
        self.demande2.reponse.provisionneSubsistance()
        self.demande2.cloture()
        self.demande2.save()

    def test_not_logged(self):
        response = self.client.get("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 302)

        response = self.client.get("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 302)

        response = self.client.post("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 302)

    def test_random_logged(self):
        self.client.force_login(self.random_user)

        response = self.client.get("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 403)

        response = self.client.get("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 404)

        response = self.client.post("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 403)

        response = self.client.post("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 404)

    def test_responsable_logged_pas_retour_spe(self):
        self.client.force_login(user=self.responsable)

        # Check 404
        response = self.client.get("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 404)

        response = self.client.post("/demande/" + str(45) + "/retour")
        self.assertEqual(response.status_code, 404)

        # Check get

        response = self.client.get("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context["retourForm"], RetourForm)
        self.assertIsInstance(response.context["retourFormSpe"], RetourNonDefiniForm)
        self.assertEqual(response.context["id_dde"], 1)

        # Formulaire invalide
        response = self.client.post("/demande/" + str(self.demande.id) + "/retour")
        self.assertEqual(response.status_code, 400)

        """
        # Il manque le champ "nb_masques_FFP2": "0"
        retour_form = {"nb_masques_chir":"0",
                            "nb_masques_textiles":"0",
                            "nb_surblouses":"0",
                            "nb_charlottes":"0",
                            "difficultes":"",
                            "amelioration":"",
                            "commentaire":"",
                             }
        response = self.client.post('/demande/' + str(self.demande.id) + '/retour',retour_form)
        self.assertEqual(response.status_code, 400)
        """

        # Formulaire correct
        retour_form = {
            "nb_masques_chir": "2",
            "nb_masques_FFP2": "4",
            "nb_masques_textiles": "0",
            "nb_surblouses": "0",
            "nb_charlottes": "0",
            "difficultes": "aucune",
            "amelioration": "",
            "commentaire": "",
        }
        self.assertEqual(self.demande.etat, "cloture")
        response = self.client.post(
            "/demande/" + str(self.demande.id) + "/retour", retour_form
        )
        self.assertEqual(response.status_code, 302)
        self.demande = Demande.objects.get(id=self.demande.id)
        self.assertEqual(self.demande.etat, "termine")
        retour = self.demande.retour
        self.assertEqual(retour.nb_charlottes, 0)
        self.assertEqual(retour.nb_masques_chir, 2)
        self.assertEqual(retour.nb_masques_FFP2, 4)
        self.assertEqual(retour.difficultes, "aucune")

        # Test réenregistrement retour
        response = self.client.post(
            "/demande/" + str(self.demande.id) + "/retour", retour_form
        )
        self.assertEqual(response.status_code, 302)

    def test_responsable_logged_retour_spe(self):
        self.client.force_login(user=self.responsable)

        # Check get

        response = self.client.get("/demande/" + str(self.demande2.id) + "/retour")
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context["retourForm"], RetourForm)
        self.assertIsInstance(response.context["retourFormSpe"], RetourDesserrementForm)
        self.assertEqual(response.context["id_dde"], 2)

        """
            Formulaires invalide
        """
        response = self.client.post("/demande/" + str(self.demande2.id) + "/retour")
        self.assertEqual(response.status_code, 400)

        # Il manque le champ "nb_masques_FFP2": "0" + les champs spécifiques
        retour_form = {
            "nb_masques_chir": "0",
            "nb_masques_textiles": "0",
            "nb_surblouses": "0",
            "nb_charlottes": "0",
            "difficultes": "",
            "amelioration": "",
            "commentaire": "",
        }
        response = self.client.post(
            "/demande/" + str(self.demande2.id) + "/retour", retour_form
        )
        self.assertEqual(response.status_code, 400)

        """
        # Il manque le champ nb_VPSP_non_CRF
        retour_form = {"nb_masques_chir": "0",
                       "nb_masques_FFP2": "0",
                       "nb_masques_textiles": "0",
                       "nb_surblouses": "0",
                       "nb_charlottes": "0",
                       "difficultes": "",
                       "amelioration": "",
                       "commentaire": "",
                       "nb_beneficiaire": "0",
                       "nb_VPSP_CRF": "0",
                       "nb_hors_VPSP": "0",
                       }
        response = self.client.post('/demande/' + str(self.demande2.id) + '/retour', retour_form)
        self.assertEqual(response.status_code, 400)
        """

        # Formulaire correct
        retour_form = {
            "nb_masques_chir": "0",
            "nb_masques_FFP2": "0",
            "nb_masques_textiles": "0",
            "nb_surblouses": "0",
            "nb_charlottes": "0",
            "difficultes": "",
            "amelioration": "",
            "commentaire": "",
            "nb_beneficiaire": "2",
            "nb_VPSP_non_CRF": "0",
            "nb_VPSP_CRF": "0",
            "nb_hors_VPSP": "0",
        }
        response = self.client.post(
            "/demande/" + str(self.demande2.id) + "/retour", retour_form
        )
        self.assertEqual(response.status_code, 302)
        retour = Demande.objects.get(id=self.demande2.id).retour
        self.assertEqual(retour.get_retour_spe().nb_VPSP_CRF, 0)
        self.assertEqual(retour.get_retour_spe().nb_beneficiaire, 2)
        self.assertEqual(retour.demande.etat, "termine")

    def test_admin_retour(self):
        self.demande3 = Demande(titre="Croix Rouge chez vous")
        self.demande3.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande3.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande3.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande3.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande3.responsable = self.responsable_profil
        self.demande3.save()
        self.demande3.validate()
        self.demande3.reponse.provisionneEffectif()
        self.demande3.reponse.provisionneVehicule()
        self.demande3.reponse.provisionneLogistique()
        self.demande3.reponse.provisionneSubsistance()
        self.demande3.cloture()
        self.demande3.save()

        self.chef = User(username="chef", password="mdp")
        self.chef.save()
        self.chef.user_permissions.add(Permission.objects.get(codename="is_chef"))
        self.chef.save()

        self.chef_profil = Profil(user=self.chef, tel="067557756")
        self.chef_profil.save()
        self.client.force_login(self.chef)

        retour_form = {
            "nb_masques_chir": "2",
            "nb_masques_FFP2": "4",
            "nb_masques_textiles": "0",
            "nb_surblouses": "0",
            "nb_charlottes": "0",
            "difficultes": "aucune",
            "amelioration": "",
            "commentaire": "",
        }
        self.assertEqual(self.demande3.etat, "cloture")
        response = self.client.post(
            "/demande/" + str(self.demande3.id) + "/retour", retour_form
        )
        self.assertEqual(response.status_code, 302)

        self.demande3 = Demande.objects.get(id=self.demande3.id)
        self.assertEqual(self.demande3.etat, "termine")
        retour = self.demande3.retour
        self.assertEqual(retour.nb_charlottes, 0)
        self.assertEqual(retour.nb_masques_chir, 2)
        self.assertEqual(retour.nb_masques_FFP2, 4)
        self.assertEqual(retour.difficultes, "aucune")
