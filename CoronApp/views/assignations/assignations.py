import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_GET, require_POST

from CoronApp.models import Reponse, Reservation
from CoronApp.models.effectif.competence import Competence
from CoronApp.models.effectif.effectif import Effectif, EffectifCRF, Effectif1J
from CoronApp.models.logistique.vehicule import Vehicule, TypeVehicule

import logging

logger = logging.getLogger(__name__)


def peut_assigner(user, reponse, type_ressource):
    if user.has_perm("CoronApp.is_chef"):
        return True
    if not user.has_perm("CoronApp.can_assign_" + type_ressource):
        return False
    elif reponse.demande.etat in ["valide", "provisione"]:
        return True
        # if not getattr(reponse,type_ressource + '_provisionnes'):
        #   return True
    return False


@login_required
@require_GET
def assign_view(request, pk, t):
    reponse = Reponse.objects.get(demande__id=pk)
    if t == "vehicules":
        if not peut_assigner(request.user, reponse, t):
            raise PermissionDenied()
        vehicules = Vehicule.objects.all()
        # On récupère les dispos des véhicules et on crée qui contient des dictionnaires {vehicule, dispo}.
        # C'est pas ouf mais j'ai pas trouvé mieux pour l'instant
        tab = [
            {
                "vehicule": vehicule,
                "dispo": vehicule.est_dispo(demande=reponse.demande),
                "deja_allouee": vehicule.est_alloue_reponse(reponse),
            }
            for vehicule in vehicules
        ]

        type_vehicule = TypeVehicule.objects.all()

        return render(
            request,
            "assignations/assignations-vehicules.html",
            {
                "reponse": reponse,
                "vehiculesEtDispos": tab,
                "type_vehicule": type_vehicule,
            },
        )

    if t == "subsistances":
        if peut_assigner(request.user, reponse, t):
            return render(
                request,
                "assignations/assignations-subsistances.html",
                {"reponse": reponse},
            )
        else:
            raise PermissionDenied
    if t == "effectifs":
        if not peut_assigner(request.user, reponse, t):
            raise PermissionDenied
        # On crée un tableau qui contient les effectifs et des informations complémentaires les concernant
        # On commence par les bénévoles croix rouge français
        effectifsEtDispos = [
            {
                "effectif": effectif,
                "dispo": effectif.est_dispo(demande=reponse.demande),
                "deja_allouee": effectif.est_alloue_reponse(reponse),
                "type": "CRF",
            }
            for effectif in EffectifCRF.objects.all()
        ]

        # On ajoute les bénévoles 1J
        effectifsEtDispos += [
            {
                "effectif": effectif,
                "dispo": effectif.est_dispo(demande=reponse.demande),
                "deja_allouee": effectif.est_alloue_reponse(reponse),
                "type": "1J",
            }
            for effectif in Effectif1J.objects.all()
        ]

        competences = Competence.objects.all()
        return render(
            request,
            "assignations/assignations-effectifs.html",
            {
                "reponse": reponse,
                "effectifsEtDispos": effectifsEtDispos,
                "competences": competences,
            },
        )

    if t == "logistique":
        if peut_assigner(request.user, reponse, t):
            return render(
                request,
                "assignations/assignations-logistique.html",
                {"reponse": reponse},
            )
        else:
            raise PermissionDenied("Vous n'avez pas le droit")
    if t == "comment":
        if request.user.has_perm("CoronApp.can_comment"):
            return render(
                request,
                "assignations/assignations-commentaire.html",
                {"reponse": reponse},
            )
        else:
            raise PermissionDenied("Vous n'avez pas le droit")

    return HttpResponse("Le type de ressource " + t + " n'existe pas", status=404)


@login_required
@require_POST
def enregistrer_commentaire_assignation(request, pk):
    parsed_json = json.loads(request.body)

    try:
        reponse = Reponse.objects.get(demande_id=pk)
    except:
        raise ObjectDoesNotExist("La réponse n'existe pas...")

    type = parsed_json["type"]
    if type == "vehicules":
        if not peut_assigner(request.user, reponse, type):
            raise PermissionDenied()

        reponse.commentaire_vehicules = parsed_json["commentaire"]
        reponse.save()
        return HttpResponse("Commentaire enregistré !", status=200)

    if type == "effectifs":
        if not peut_assigner(request.user, reponse, type):
            raise PermissionDenied()

        reponse.commentaire_effectifs = parsed_json["commentaire"]
        reponse.save()
        return HttpResponse("Commentaire enregistré !", status=200)

    return HttpResponse("Le type de ressource " + type + " n'existe pas", status=400)


@login_required
@require_POST
def finir_assignation(request, pk):
    # On récupère la réponse associée à la demande
    try:
        reponse = Reponse.objects.get(demande_id=pk)
    except:
        raise ObjectDoesNotExist("La réponse n'existe pas...")

    parsed_json = json.loads(request.body)
    type = parsed_json["type"]
    if type == "vehicules":
        if not peut_assigner(request.user, reponse, type):
            raise PermissionDenied("Vous n'avez pas les droits ")

        reponse.commentaire_vehicules = parsed_json["commentaire"]
        reponse.provisionneVehicule()
        reponse.save()
        return HttpResponse("Véhicules provisionnés", status=200)

    if type == "effectifs":
        if not peut_assigner(request.user, reponse, type):
            raise PermissionDenied("Vous n'avez pas les droits ")
        reponse.commentaire_effectifs = parsed_json["commentaire"]
        reponse.provisionneEffectif()
        reponse.save()
        return HttpResponse("Effectifs provisionnés", status=200)

    return HttpResponse("Le type de ressource " + type + " n'existe pas", status=400)


@login_required
@require_POST
def affecter_ressource(request, pk):
    # On charge le JSON
    parsed_json = json.loads(request.body)
    type = parsed_json["type"]

    # On va chercher la reponse associée à la demande
    reponse = Reponse.objects.get(demande_id=pk)

    ressource_id = int(parsed_json["ressource_id"])
    # Pour l'instant 1 (pour véhicules et effectifs)
    quantite = 1
    # La ressource que l'on veut allouer
    ressource = None

    # On récupère la ressource en question
    if type == "vehicules":
        if not peut_assigner(request.user, reponse, type):
            raise PermissionDenied("Vous n'avez pas les droits ")
        try:
            # On récupère la ressource
            ressource = Vehicule.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist(
                "Une demande d'assignation a été faite sur un véhicule inexistant"
            )

    if type == "effectifCRF":
        if not peut_assigner(request.user, reponse, "effectifs"):
            raise PermissionDenied("Vous n'avez pas les droits ")
        try:
            # On récupère la ressource
            ressource = EffectifCRF.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist(
                "Une demande d'assignation a été faite sur un effectif inexistant"
            )

    if type == "effectif1J":
        if not peut_assigner(request.user, reponse, "effectifs"):
            raise PermissionDenied("Vous n'avez pas les droits ")
        try:
            # On récupère la ressource
            ressource = Effectif1J.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist(
                "Une demande d'assignation a été faite sur un effectif inexistant"
            )
    # Si l'on a pas récupérer l'objet (il n'est pas d'un type connu)
    if ressource == None:
        return HttpResponse("Le type " + type + " n'est pas connu", status=400)

    # Si la ressource est déja allouée (c'est pas normal !)
    # Comportement à mettre à jour pour les matériels (quantité > 1)
    if ressource.est_alloue_reponse(reponse):
        logger.warning(
            request.user.last_name
            + request.user.first_name
            + " a demandé à allouer la ressource de type "
            + type
            + " sur une mission où elle était déja alloué"
        )
        return HttpResponse(
            "La ressource était déja affecté sur la mission", status=400
        )

    if not ressource.est_dispo(demande=reponse.demande):
        return HttpResponse(
            "Impossible d'allouer la ressource, elle est indisponible.", status=400
        )

    reservation = Reservation(
        ressource=ressource.ressource, reponse=reponse, quantite=quantite
    )
    reservation.save()

    return HttpResponse("La ressource a bien été affectée sur la mission", status=200)


@login_required
@require_POST
def desaffecter_ressource(request, pk):
    parsed_json = json.loads(request.body)

    reponse = Reponse.objects.get(demande_id=pk)

    ressource_id = int(parsed_json["ressource_id"])
    # La ressource que l'on veut desaffecter
    ressource = None
    type = parsed_json["type"]
    type_permission = type
    if type in ["effectifCRF", "effectif1J"]:
        type_permission = "effectifs"

    if not peut_assigner(request.user, reponse, type):
        raise PermissionDenied()

    if type == "vehicules":
        try:
            ressource = Vehicule.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist(
                "Une demande d'assignation a été faite sur un véhicule inexistant"
            )

    if type == "effectifCRF":
        try:
            # On récupère la ressource
            ressource = EffectifCRF.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist("Effectif inexistant")

    if type == "effectif1J":
        try:
            # On récupère la ressource
            ressource = Effectif1J.objects.get(id=ressource_id)
        except:
            raise ObjectDoesNotExist("Effectif inexistant")

    # Si l'on a pas récupérer l'objet (il n'est pas d'un type connu)
    if ressource == None:
        return HttpResponse("Le type " + type + " n'est pas connu", status=400)

    # Si la ressource est n'est pas allouée (c'est pas normal !)
    if not ressource.est_alloue_reponse(reponse):
        logger.warning(
            request.user.last_name
            + request.user.first_name
            + " a demandé à dé-allouer la ressource sur une mission où elle n'était pas alloué, il y a du avoir un pb dans le front"
        )
        return HttpResponse(
            "La ressource n'était pas affectée sur la mission", status=400
        )

    reservation = ressource.get_reservation_commune(reponse)
    reservation.delete()
    return HttpResponse("La ressource a bien été retirée de la mission", status=200)


"""
Cette fonction est vouée à disparaitre, c'est celle qui gérait les assignations auparavant
"""


@login_required
@require_POST
def set_assign(request, pk):
    if request.user.is_authenticated:
        parsed_json = json.loads(request.body)
        reponse = get_object_or_404(Reponse, demande_id=pk)

        type = parsed_json["type"]
        if type == "subsistances":
            if peut_assigner(request.user, reponse, type):
                reponse.subsistances = parsed_json["pld"]
                if parsed_json["end"]:
                    reponse.provisionneSubsistance()
                reponse.save()
                return HttpResponse(status=201)

        if type == "logistique":
            if peut_assigner(request.user, reponse, type):
                reponse.logistique = parsed_json["pld"]
                if parsed_json["end"]:
                    reponse.provisionneLogistique()
                reponse.save()
                return HttpResponse(status=201)
            raise PermissionDenied

        if type == "comment":
            if request.user.has_perm("CoronApp.can_comment"):
                reponse = Reponse.objects.get(demande_id=pk)
                reponse.commentaires = parsed_json["pld"]
                reponse.save()
                return HttpResponse(status=201)
            raise PermissionDenied

    raise PermissionDenied()
