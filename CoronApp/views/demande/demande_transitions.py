from django.contrib.auth.decorators import permission_required, login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import JsonResponse

from CoronApp.models import Demande


@login_required
@permission_required("CoronApp.can_validate")
def validate(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.validate()
        demande.save()
        return JsonResponse({"success": True, "error": None})
    except ObjectDoesNotExist:
        return JsonResponse({"error": "La demande n'existe pas..."}, status=500)
    except IntegrityError:
        return JsonResponse(
            {
                "error": "Un réponse liée à cette demande existe déja ! (Contactez un admin)"
            },
            status=500,
        )


@login_required
@permission_required("CoronApp.can_validate")
def refuse(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.refuse()
        demande.save()
        return JsonResponse({"success": True, "error": None})
    except ObjectDoesNotExist:
        return JsonResponse({"error": "La demande n'existe pas..."}, status=500)


@login_required
@permission_required("CoronApp.can_cloture")
def cloture(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.cloture()
        demande.save()
        return JsonResponse({"success": True, "error": None})
    except ObjectDoesNotExist:
        return JsonResponse({"success": False, "error": "La demande n'existe pas..."})


# Doit être responsable mais c'est pas une perm
# Verifier l'identité en amont
def termine(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.termine()
        demande.save()
        return JsonResponse({"success": True, "error": None})
    except ObjectDoesNotExist:
        return JsonResponse({"success": False, "error": "La demande n'existe pas..."})


@login_required
@permission_required("CoronApp.can_archive")
def archive(request, pk):
    try:
        demande = Demande.objects.get(id=pk)
        demande.archive()
        demande.save()
        return JsonResponse({"success": True, "error": None})
    except ObjectDoesNotExist:
        return JsonResponse({"success": False, "error": "La demande n'existe pas..."})
