import logging

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.shortcuts import render

from CoronApp.views.imports_excel.imports_benevoles import process_excel_benevoles
from CoronApp.views.imports_excel.imports_global import ImportData
from CoronApp.views.imports_excel.imports_vehicules import process_excel_vehicules

logger = logging.getLogger(__name__)


@login_required
def imports_view(request):
    """
    Réceptionne toutes les requêtes traitant d'un import, en dedistribuant les données vers d'autres vues de traitement
    """

    # Permission spéciique pour les imports
    if not request.user.has_perm("CoronApp.peut_importer"):
        raise PermissionDenied(
            "Vous n'avez pas la permission d'importer des fichiers Excel."
            "Reportez-vous à votre référent si vous pensez que c'est une erreur."
        )

    importer = ImportData()

    if request.method == "POST":
        try:
            excel_file = request.FILES["excel_file"]
            logger.debug("Import de l'excel: " + excel_file.name)

            if request.POST["type"] == "vehicule":
                process_excel_vehicules(excel_file, importer)

            logger.debug("\n\n Exceptions")
            logger.debug(importer.error_list)

            logger.debug("\n\n Ajouts")
            logger.debug(importer.add_list)

            logger.debug("\n\n Ignorés")
            logger.debug(importer.untouched_list)

            return render(
                request,
                "imports/import_succes.html",
                {
                    "error_list": importer.error_list,
                    "add_list": importer.add_list,
                    "untouched_list": importer.untouched_list,
                },
            )
        except Exception:
            raise ObjectDoesNotExist("Fichier excel non fourni")

    return render(request, "imports/import_form.html")
