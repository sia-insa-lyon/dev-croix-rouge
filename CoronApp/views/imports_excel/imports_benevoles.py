from pandas import *

from CoronApp.models.effectif.competence import Competence, TypeCompetence
from CoronApp.models.effectif.effectif import EffectifCRF, Effectif1J
from CoronApp.views.imports_excel.imports_global import (
    DataToImport,
    ImportDataUtility,
    ImportData,
    ListToImport,
)


class CompetenceToImport(DataToImport):
    def process_data(self, row, key):
        comp_obj = Competence(
            competenceType=ImportDataUtility.process_data_object(
                row, "TYPE", TypeCompetence
            ),
            denomination=key,
            description=ImportDataUtility.process_data(row, "DESCRIPTION"),
            permissionBNVCRF=True,  # TODO permissionBNVCRF et permissionBNV1J ?
            permissionBNV1J=True,
        )
        comp_obj.save()


class EffectifToImport(DataToImport):
    def process_competences(self, data: str):
        """Permet de sortir un tableau d'objets Competence pour un effectif"""

        list_comp = []

        for comp in data.split(ImportDataUtility.COMP_SEPARATOR):
            if Competence.objects.filter(denomination=comp).exists():
                comp_obj = Competence.objects.get(denomination=comp)

                # Vérifie les droits
                if self.checkPrivilege(
                    comp_obj.permissionBNVCRF, comp_obj.permissionBNV1J
                ):
                    list_comp.append(Competence.objects.get(denomination=comp))
                else:
                    raise Exception(
                        comp
                        + " n'est pas valable pour ce type d'effectif : "
                        + "BNVCRF."
                        if (type(self).__name__ == EffectifBNVCRFToImport.__name__)
                        else "BNV1J."
                    )

            else:
                raise Exception(comp + " est une compétence inexistante.")

        return list_comp

    def checkPrivilege(self, permissionBNVCRF, permissionBNV1J):
        pass


class EffectifBNVCRFToImport(EffectifToImport):
    def process_data(self, row, key):
        effect_crf = EffectifCRF(
            nom=ImportDataUtility.process_data(row, "Nom"),
            prenom=ImportDataUtility.process_data(row, "Prénom"),
            telephone=str(ImportDataUtility.process_data(row, "Téléphone")),
            email=key,
            commentaire=ImportDataUtility.process_data(row, "Commentaire"),
            NIVOL=ImportDataUtility.process_data(row, "NIVOL"),
        )
        effect_crf.save()
        effect_crf.competences.add(
            *self.process_competences(getattr(row, "Compétences"))
        )
        effect_crf.save()

    def checkPrivilege(self, permissionBNVCRF, permissionBNV1J):
        return permissionBNVCRF


class Effectif1JCRFToImport(EffectifToImport):
    def process_data(self, row, key):
        effect_1J = Effectif1J(
            nom=ImportDataUtility.process_data(row, "Nom"),
            prenom=ImportDataUtility.process_data(row, "Prénom"),
            telephone=str(ImportDataUtility.process_data(row, "Téléphone")),
            email=key,
            commentaire=ImportDataUtility.process_data(row, "Commentaire"),
        )
        effect_1J.save()
        effect_1J.competences.add(
            *self.process_competences(getattr(row, "Compétences"))
        )
        effect_1J.save()

    def checkPrivilege(self, permissionBNVCRF, permissionBNV1J):
        return permissionBNV1J


def process_excel_benevoles(excel_file, importer: ImportData):
    """Process l'excel, fonction principale"""

    # Essaye d'ouvrir toutes les pages
    try:
        df_effectifs_bnv_crf = pandas.read_excel(
            excel_file, sheet_name="EFFECTIFS - BNV CRF"
        )
        df_effectifs_competences = pandas.read_excel(
            excel_file, sheet_name="EFFECTIFS - COMPETENCES"
        )
        df_effectifs_bnv_1J = pandas.read_excel(
            excel_file, sheet_name="EFFECTIFS - BNV 1J"
        )
    except Exception as exception:
        importer.error_list.append(exception)
        return

    # Traitement des noms de colonnes
    for unsupported_char in ImportDataUtility.UNSUPPORTED_CHAR:
        df_effectifs_bnv_crf.columns = df_effectifs_bnv_crf.columns.str.replace(
            unsupported_char, "_"
        )
        df_effectifs_bnv_1J.columns = df_effectifs_bnv_1J.columns.str.replace(
            unsupported_char, "_"
        )
        df_effectifs_competences.columns = df_effectifs_competences.columns.str.replace(
            unsupported_char, "_"
        )

    # Import des types de compétences
    # page, nom de la colonne de l'excel contenant la pk, attribut pk django, objet à créer
    c_list = ListToImport(df_effectifs_competences, "TYPE", "label", TypeCompetence)
    importer.process_list(c_list)

    # Import des compétences
    c = CompetenceToImport(
        df_effectifs_competences, "DENOMINATION", "denomination", Competence
    )
    importer.process_object(c)

    # Import des effectifs BNV
    e_bnv = EffectifBNVCRFToImport(df_effectifs_bnv_crf, "E_mail", "email", EffectifCRF)
    importer.process_object(e_bnv)

    # Import des effectifs 1J
    e_1j = Effectif1JCRFToImport(df_effectifs_bnv_1J, "E_mail", "email", Effectif1J)
    importer.process_object(e_1j)
