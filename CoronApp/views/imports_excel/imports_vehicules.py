import pandas

from CoronApp.models.logistique.lieustockage import LieuStockage
from CoronApp.models.logistique.vehicule import (
    TypeVehicule,
    CategorieVehicule,
    Vehicule,
)
from CoronApp.views.imports_excel.imports_global import (
    DataToImport,
    ImportDataUtility,
    ImportData,
    ListToImport,
)


class VehiculeImportData(DataToImport):
    def process_data(self, row, key):
        vehicule = Vehicule(
            nbPlacesNormales=0
            if ImportDataUtility.process_data(row, "PLACES_NORMAL") is None
            else ImportDataUtility.process_data(row, "PLACES_NORMAL"),
            nbPlacesCovid=0
            if ImportDataUtility.process_data(row, "PLACES_COVID") is None
            else ImportDataUtility.process_data(row, "PLACES_COVID"),
            immatriculation=ImportDataUtility.process_data(row, "IMMATRICULATION"),
            typeVehicule=ImportDataUtility.process_data_object(
                row, "TYPE", TypeVehicule
            ),
            categorieVehicule=ImportDataUtility.process_data_object(
                row, "CATEGORIE", CategorieVehicule
            ),
            denomination=key,
            remarque=ImportDataUtility.process_data(row, "REMARQUES"),
            lieu=ImportDataUtility.process_data_object_or_None(
                row, "LIEU_DE_PARC", LieuStockage
            ),
            qteTotal=1,
            qteDispo=0
            if ImportDataUtility.process_data(row, "ETAT") == "INDISPO"
            else 1,
            # TODO catégorie vehicule?
        )
        vehicule.save()


def process_excel_vehicules(excel_file, importer: ImportData):
    """Process l'excel, fonction principale"""

    # Essaye d'ouvrir toutes les pages
    try:
        df_parc_auto = pandas.read_excel(excel_file)
    except Exception as exception:
        importer.error_list.append(exception)
        return

    # Traitement des noms de colonnes
    for unsupported_char in ImportDataUtility.UNSUPPORTED_CHAR:
        df_parc_auto.columns = df_parc_auto.columns.str.replace(unsupported_char, "_")

    # Import des types de véhicules
    type_vehicule = ListToImport(df_parc_auto, "TYPE", "label", TypeVehicule)
    importer.process_list(type_vehicule)

    # Import des catégories de véhicules
    cat_vehicule = ListToImport(df_parc_auto, "CATEGORIE", "label", CategorieVehicule)
    importer.process_list(cat_vehicule)

    # Import des lieux de stockage
    lieu_vehicule = ListToImport(df_parc_auto, "LIEU_DE_PARC", "label", LieuStockage)
    importer.process_list(lieu_vehicule)

    # Import du parc auto
    vehicule = VehiculeImportData(df_parc_auto, "INDICATIF", "denomination", Vehicule)
    importer.process_object(vehicule)
