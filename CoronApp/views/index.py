from django.conf import settings
from django.shortcuts import render


def index(request):
    return render(request, "index.html", context={"version": settings.APP_VERSION})
