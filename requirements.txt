asgiref==3.2.7
dj-database-url==0.5.0
Django==3.0.5
django-crispy-forms==1.9.1
django-fsm==2.7.0
gunicorn==20.0.4
psycopg2-binary==2.8.5
pytz==2020.1
sqlparse==0.3.1
phonenumbers==8.12.4
django-phonenumber-field==4.0.0
coverage==5.1
django-modeladmin-reorder==0.3.1
xlrd==1.2.0
pandas==1.0.4
