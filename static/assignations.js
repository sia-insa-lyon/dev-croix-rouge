
    // Génère le code HTML pour le bouton d'ajout d'une ressource en fonction de l'id de la ressource et de son type
    // Pas convaincu de la nécessité d'une fonction néanmoins ^^
    function genererHTMLBoutonAjout(id,type) {
        return '<button class="btn btn-outline-success" onclick="affecterRessource(' + id + ',\''+ type + '\')">Affecter</button>';
    }
    function genererHTMLLoader() {
        return '<div class="spinner-border" role="status"><span class="sr-only">Loading</span></div>'
    }
    function genererHTMLBoutonSuppr(id,type) {
        return '<button class="btn btn-outline-danger" onclick="desaffecterRessource(' + id +',\''+ type + '\')">Désaffecter</button>';
    }



    function sendPostRequest(data,url) {
         $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        });
        return $.post(url, JSON.stringify(data));
     }
